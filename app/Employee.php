<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Employees' Model
 */
class Employee extends Model
{

	/**
	 * The attributes that are mass assignable
	 * @var array
	 */
	protected $fillable = [
		'nombre',
		'apellido_p',
		'apellido_m',
		'correo',
		'telefono',
		'cargo',
	];

	/**
   * The attributes excluded from the model's JSON form
   * @var array
   */
	protected $hidden = [

	];

	/**
	 * Get the route key for the model
	 * 
	 * @return string
	 */
	public function getRouteKeyName() {
		return 'slug';
	}
}
?>
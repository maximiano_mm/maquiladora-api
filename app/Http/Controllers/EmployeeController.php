<?php
namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

/**
 * Employees' Controller
 */
class EmployeeController extends Controller
{
	/**
	 * Muestra el listado seccionado por $numberPerPage de los empleados
	 * @param int $numberPerPage
	 * @return \Illuminate\Http\Response
	 */
	public function index($numberPerPage)
	{
		# code...
		$employees = Employee::simplePaginate($numberPerPage);
		return response()->json($employees, 200);
	}

	/**
	 * Cuenta el numero de registros en la tabla Empleados
	 * @return \Illuminate\Http\Response
	 */
	public function count()
	{
		# code...
		$total = Employee::count();
		return response()->json($total);
	}

	/**
	 * Muestra un empleado en específico
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		# code...
    $employee = Employee::find($id);
    return response()->json($employee, 200);
	}

	/**
	 * Realiza una busqueda en tiempo real
	 * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
	 */
	public function search(Request $request)
	{
		# code...
		$searchItems = $request->input('search');
		$resultItems = Employee::where('nombre', 'LIKE', "%$searchItems%")->paginate(10);
		return response()->json($resultItems);
	}

	/**
	 * 
	 */
	public function store(Request $request)
	{
		# code...
		# Validación de los campos
		$this->validate($request, [
      'nombre' => 'required|max: 20',
      'apellido_p' => 'required|max: 15',
      'apellido_m' => 'required|max: 15',
      'correo' => 'required|email|max: 30',
      'telefono' => 'required|max: 15',
      'cargo' => 'required|max: 15',
      'foto' => 'required|image',
      'slug' => 'required'
    ]);

    if ($request->hasFile('foto')) {
    	# code...
    	$file = $request->file('foto');
      $name_file = time().$file->getClientOriginalName();
      $file->move(public_path().'/images/', $name_file);
    }

		# Captura y almacenamiento de los datos 
    $employee = new Employee();
    $employee->nombre = $request->input('nombre');
    $employee->apellido_p = $request->input('apellido_p');
    $employee->apellido_m = $request->input('apellido_m');
    $employee->correo = $request->input('correo');
    $employee->telefono = $request->input('telefono');
    $employee->cargo =$request->input('cargo');
    $employee->foto = $name_file;
    $employee->slug = $request->input('slug');
    $employee->save();
 
    return response()->json([$employee], 201);
	}

	/**
   * Actualiza un empleado por su id
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
	public function update($id, Request $request)
	{
		# code...
		/*$requestData = $request->all();
    $employee = Employee::findOrFail($id);

    if($request->hasFile('foto')) {
			$file = $request->file('foto');
      $name_file = time().$file->getClientOriginalName();
      $employee->avatar = $name_file;
      $file->move(public_path().'/images/', $name_file);

       $requestData['foto'] = "/public/{$name_file}";
		}*/

		$employee->update($requestData);

		/*$employee = Employee::findOrFail($id);
    $employee->fill($request->all());
        $employee->save();
    return response()->json($employee);


		//$employee->fill($request->all());
		/*$employee->fill($request->except('foto'));

		if($request->hasFile('foto')) {
			$file = $request->file('foto');
      $name_file = time().$file->getClientOriginalName();
      $employee->avatar = $name_file;
      $file->move(public_path().'/images/', $name_file);
		}

		$employee->save();

    /*$employee->fill($request->except('foto'));
    if ($request->hasFile('foto')) {
    	# code...
    	$old_file = public_path().'/images/'.$employee->foto;
    	File::delete($old_file);
    	# code...
    	$file = $request->file('foto');
      $new_file = time().$file->getClientOriginalName();
      $employee->foto = $new_file;
      $file->move(public_path().'/images/', $new_file);
    }*/

 		$employee = Employee::findOrFail($id);
 		$employee->fill($request->all());
    $employee->save();

    return response()->json(['message' => 'Employee updated succesfully'], 200);
	}

	/**
   * Remueve un empleado por su id
   * @param int $id
   * @return \Illuminate\Http\Response
   */
	public function destroy($id)
	{
		# code...
    $employee = Employee::findOrFail($id);
    $file_path = public_path().'/images/'.$employee->foto;
    File::delete($file_path);
    $employee->delete();

    return response()->json(['message' => 'Employee deleted succesfully'], 200);
	}
}
?>
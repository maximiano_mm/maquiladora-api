<?php
use Illuminate\Support\Str;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
  return $router->app->version();
});

/**
 * Gives an APP_KEY for the .env file
 * @return string
 */
$router->get('/key', function () {
	return str_random(32);
});

$router->get('/public', function () {
	$path = public_path();
	return $path;
});

$router->get('/slug', function () {
	$slug = Str::slug('Maximiano Mendez Morales', '-');
	return $slug;
});

/**
 * API Routes
 */
$router->group(['prefix' => 'api/v1'], function () use ($router) {
	// Empleados endpoints
	$router->get('/employees/pages/{number}', ['uses' => 'EmployeeController@index']);
	$router->post('employees/search', ['uses' => 'EmployeeController@search']);
	$router->get('employees/count', ['uses' => 'EmployeeController@count']);
	$router->get('/employees/{id}', ['uses' => 'EmployeeController@show']);
	$router->post('/employees', ['uses' => 'EmployeeController@store']);
	$router->put('/employees/{id}', ['uses' => 'EmployeeController@update']);
	$router->delete('/employees/{id}', ['uses' => 'EmployeeController@destroy']);

	// Usuarios endpoints
});
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('employees', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('nombre', 25);
      $table->string('apellido_p', 25);
      $table->string('apellido_m', 25);
      $table->string('correo', 30);
      $table->string('telefono', 15);
      $table->string('cargo', 35);
      $table->string('foto', 255);
      $table->string('slug', 15)->unique();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('employees');
  }
}
